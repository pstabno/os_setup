#!/bin/bash
set location = pwd

## Install Packages
sudo pacman -Syu
sudo pacman -Sy --needed tmux ack
the_silver_searcher git wget curl sed ctags mc zsh llvm clang clang-tools-extra
python-pip

ln -s .tmux.conf ~/.tmux.conf

## Install Vim
echo "New configuration is used!"
ln -s  ~/.vimrc  $location/.vimrc

## Install Oh-My-Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cd ~/.oh-my-zsh/themes
wget http://raw.github.com/zakaziko99/agnosterzak-ohmyzsh-theme/master/agnosterzak.zsh-theme
cd $location
ln -s $location/.zshrc ~/.zshrc  

# required dirs
cd ~/.vim
mkdir tmp
cd tmp/
mkdir swap backup undo
cd

# install Plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +PlugInstall +q +q

cp -R .fonts ~
#cp -R i3 ~/.config

# install YCM
#cd ~/.vim/bundle/YouCompleteMe
#./install.py --clang-completer
#cd $location
#cp vim_files/.ycm_extra_conf.py ~/.vim/.ycm_extra_conf.py
#cp -r vim_files/c-support ~/.vim/

chsh -s /bin/zsh
